<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accounts;
use DB, Exception, Log;

class APIAccountsController extends Controller
{
	
	private $api_key = 'gzPMgs3nhzGroYdZBbSex9szVQV8NNei';
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
			
			if(!$request->api_key || $request->api_key <> $this->api_key)
				throw new Exception(trans('instaServices.unauthenticated'));
			
			$status	 	= $request->status ? $request->status : '1';
			$order		= $request->order ? $request->order : 'id';
			$order_arg	= $request->order_arg ? $request->order_arg : 'asc';
			$limit		= $request->limit ? $request->limit : '-1';
			
			$accounts = Accounts::where('status', $status)
								  ->orderBy($order, $order_arg)
								  ->limit($limit)
								  ->get();
		
        	return response()->json(['status' => true, 'result' => $accounts]);
		
		} catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => false, 'result' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
    	
		try {
			
			if(!$request->api_key || $request->api_key <> $this->api_key)
				throw new Exception(trans('instaServices.unauthenticated'));
			
			$account = Accounts::findOrFail($id);
		
        	return response()->json(['status' => true, 'result' => $account]);
		
		} catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => false, 'result' => $e->getMessage()]);
        }
		
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
			
			if(!$request->api_key || $request->api_key <> $this->api_key)
				throw new Exception(trans('instaServices.unauthenticated'));
			
			$account 	= Accounts::findOrFail($id);
			$account->update($request->except('api_key'));
		
        	return response()->json(['status' => true, 'result' => $account]);
		
		} catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => false, 'result' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

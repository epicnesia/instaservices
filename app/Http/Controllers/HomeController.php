<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Accounts;
use Carbon\Carbon;
use DB, Exception, Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
        	
			$this->authorize('seeIndex', new Accounts());
        	
			$accounts	= Accounts::orderBy('created_at', 'desc')->paginate(30);
			
			return view('home')->with('accounts', $accounts);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('instaServices.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

	/**
     * Download all resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadAll()
    {
        try {
        	
			$this->authorize('seeIndex', new Accounts());
			
			$accounts	= Accounts::all();
			
			if($accounts->count() > 0){
				$contents	= '';
				foreach ($accounts as $account) {
					$contents .= $account->username.':'.$account->password.PHP_EOL;
				}
				$filename	= 'InstaServices-'.str_replace(" ", "-", (Carbon::now())).'.txt';
				$headers	= ['Content-type'=>'text/plain', 'Content-Disposition'=>sprintf('attachment; filename="%s"', $filename)];
				
				return response()->make($contents, 200, $headers);
			} else {
				return redirect('/home')->with('error', trans('instaServices.No account to download'));
			}
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('instaServices.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

	/**
     * Delete all resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteAll()
    {
        try {
        	
			$this->authorize('seeIndex', new Accounts());
			
			DB::beginTransaction();
			
			Accounts::truncate();
			
			DB::commit();
			
			return redirect('/home')->with('success', trans('instaServices.All accounts deleted successfully!'));
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('instaServices.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }
	
}

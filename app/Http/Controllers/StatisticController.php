<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Accounts;
use Carbon\Carbon;
use DB, Exception, Log;

class StatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
        	
			$this->authorize('seeIndex', new Accounts());
			
			$dailyStat	= self::getStatDaily();
			$weeklyStat	= self::getStatWeekly();
			$monthlyStat= self::getStatMonthly();
			
			return view('statistic')->with('dailyLabel', $dailyStat['label'])->with('dailyData', $dailyStat['data'])
									->with('weeklyLabel', $weeklyStat['label'])->with('weeklyData', $weeklyStat['data'])
									->with('monthlyLabel', $monthlyStat['label'])->with('monthlyData', $monthlyStat['data']);
				
    	} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('instaServices.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getLine() . $e->getMessage());
        }
    }
	
	// Get daily statistic data
	public static function getStatDaily()
	{
		
		$totalDays	= date('t');
		
		$label		= '[';
		$data		= '[[';
		for($i=1;$i<=$totalDays;$i++){
			$day	= strlen($i) < 2 ? '0'.$i : $i;
			$label  .= '"'.$i.'",';
			$data	.= '"'.Accounts::whereDate('created_at', date('Y').'-'.date('m').'-'.$i)->count().'",';
		}
		$label		.= ']';
		$data		.= ']]';
		
		return ['label'=>$label, 'data'=>$data];		
		
	}
	
	// Get weekly statistic data
	public static function getStatWeekly()
	{
		
		$totalWeek	= self::weeks_in_month(date('Y'), date('n'), 1);
		
		$label		= '[';
		$data		= '[[';
		for($i=1;$i<=$totalWeek;$i++){
			$day		= $i > 1 ? 1 + ($i*7) : 1 + 7;
			
			$startWeek 	= Carbon::create(date('Y'), date('n'), $day)->startOfWeek();
			$endWeek	= Carbon::create(date('Y'), date('n'), $day)->endOfWeek();
			
			$label  	.= '"Week '.$i.'",';
			$data		.= '"'.Accounts::whereBetween('created_at', [$startWeek, $endWeek])->count().'",';
		}
		$label		.= ']';
		$data		.= ']]';
		
		return ['label'=>$label, 'data'=>$data];		
		
	}
	
	// Get monthly statistic data
	public static function getStatMonthly()
	{
		
		$months	= ['1'=>'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		
		$label		= '[';
		$data		= '[[';
		for($i=1;$i<=count($months);$i++){
			$month	= strlen($i) < 2 ? '0'.$i : $i;
			$label  .= '"'.$months[$i].'",';
			$data	.= '"'.Accounts::whereMonth('created_at', $month)->count().'",';
		}
		$label		.= ']';
		$data		.= ']]';
		
		return ['label'=>$label, 'data'=>$data];	
		
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/**
	 * Return the total number of weeks of a given month.
   	 * @param int $year
   	 * @param int $month
   	 * @param int $start_day_of_week (0=Sunday ... 6=Saturday)
   	 * @return int
   	 */
	public static function weeks_in_month($year, $month, $start_day_of_week='0')
	{
	    // Total number of days in the given month.
	    $num_of_days = date("t", mktime(0,0,0,$month,1,$year));
	 
	    // Count the number of times it hits $start_day_of_week.
	    $num_of_weeks = 0;
	    for($i=1; $i<=$num_of_days; $i++)
	    {
	      $day_of_week = date('w', mktime(0,0,0,$month,$i,$year));
	      if($day_of_week==$start_day_of_week)
	        $num_of_weeks++;
	    }
	 
	    return $num_of_weeks;
	}
}

<?php

use Illuminate\Database\Seeder;
use Kodeine\Acl\Models\Eloquent\Role;
use Kodeine\Acl\Models\Permission;

class AclRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
    	$role = new Role();
		$roleAdmin = $role->create([
		    'name' => 'Admin',
		    'slug' => 'admin',
		    'description' => 'manage administrator privileges'
		]);
		
    }
}

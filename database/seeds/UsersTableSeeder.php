<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Add admin user
        $adminId = DB::table('users')->insertGetId([
                                                       'username'   => 'instagersadmminn',
                                                       'name' 		=> 'Instagersadmminn',
                                                       'email'      => 'instagersadmminn@instagers.com',
                                                       'password'   => bcrypt('mYIbrlye4Sn8'),
                                                       'created_at' => date('Y-m-d H:i:s'),
                                                       'updated_at' => date('Y-m-d H:i:s'),
                                                   ]);
        $admin   = User::find($adminId);
        $admin->assignRole('admin'); // Assign Role


    }
}

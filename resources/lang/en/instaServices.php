<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2017/04/26 03:25:47 
*************************************************************************/

return array (
  'All accounts deleted successfully!' => 'All accounts deleted successfully!',
  'All good you will get your followers soon!' => 'All good you will get your followers soon!',
  'Contact Us' => 'Contact Us',
  'Created At' => 'Created At',
  'Dashboard' => 'Dashboard',
  'Delete All' => 'Delete All',
  'Do you want to delete all account?' => 'Do you want to delete all account?',
  'Download All' => 'Download All',
  'Download an app and get 100 Real followers now!!!' => 'Download an app and get 100 Real followers now!!!',
  'IP Address' => 'IP Address',
  'Instagram Password' => 'Instagram Password',
  'Instagram Username' => 'Instagram Username',
  'No account to download' => 'No account to download',
  'Password' => 'Password',
  'Submit' => 'Submit',
  'This Action is Unauthorized' => 'This Action is Unauthorized',
  'Username' => 'Username',
  'Your username does not exist on instagram!' => 'Your username does not exist on instagram!',
  'error' => 'Error',
  'info' => 'info',
  'success' => 'Success',
  'warning' => 'Warning',
  'Daily Statistic' => 'Daily Statistic',
  'Email' => 'Email',
  'Message' => 'Message',
  'Monthly Statistic' => 'Monthly Statistic',
  'Name' => 'Name',
  'Statistic' => 'Statistic',
  'Thanks to contact us!' => 'Thanks to contact us!',
  'Weekly Statistic' => 'Weekly Statistic',
  'unauthenticated' => 'unauthenticated',
);
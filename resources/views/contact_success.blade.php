<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		@include('layouts.home_head')
	</head>
	<body>

		<section id="success" class="success">
			@include('layouts.home_navbar')
			<div class="container">
				<div class="row">
					<div class="banner-info col-sm-offset-2 col-sm-8">
						<div class="banner-text text-center">
							@include('layouts.notifications')
							<h1 class="white">{{ trans('instaServices.Thanks to contact us!') }}</h1>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}"></script>
		<script src="{{ asset('js/custom.js') }}"></script>

	</body>
</html>

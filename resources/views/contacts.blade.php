<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		@include('layouts.home_head')
	</head>
	<body>

		<section id="contacts" class="contacts">
			@include('layouts.home_navbar')
			<div class="container">
				<div class="row">
					@include('layouts.notifications')
					<div class="panel panel-default">
						<div class="panel-heading">{{ trans('instaServices.Contact Us') }}</div>
						<div class="panel-body">
							<form action="{{ action('ContactsController@store') }}" method="POST" class="form-horizontal">
								{{ csrf_field() }}
								<div class="form-group">
									<label for="name" class="col-sm-2 control-label">{{ trans('instaServices.Name') }}</label>
									<div class="col-sm-5">
										<input type="text" name="name" class="form-control" id="name" placeholder="{{ trans('instaServices.Name') }}">
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="col-sm-2 control-label">{{ trans('instaServices.Email') }}</label>
									<div class="col-sm-5">
										<input type="email" name="name" class="form-control" id="email" placeholder="{{ trans('instaServices.Email') }}">
									</div>
								</div>
								<div class="form-group">
									<label for="message" class="col-sm-2 control-label">{{ trans('instaServices.Message') }}</label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="10" name="message" placeholder="{{ trans('instaServices.Message') }}"></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="submit" class="btn btn-default">
											{{ trans('instaServices.Submit') }}
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}"></script>
		<script src="{{ asset('js/custom.js') }}"></script>

	</body>
</html>

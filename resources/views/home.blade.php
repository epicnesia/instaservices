@extends('layouts.app')

@section('content')
<div class="container">
   	@include('layouts.notifications')
	
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-body">
					<form class="delete" action="{{ action('HomeController@deleteAll') }}" method="POST">
						<div class="btn-group" role="group">
							<a href="{{ action('HomeController@downloadAll') }}" class="btn btn-info">{{ trans('instaServices.Download All') }}</a>
					        <input type="hidden" name="_method" value="DELETE">
					        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
					        <input type="submit" class="btn btn-danger" value="{{ trans('instaServices.Delete All') }}">
						</div>
					</form>
				</div>
			</div>
			
		</div>
	</div>
	
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('instaServices.Dashboard') }}</div>

                <div class="panel-body">
                    @if($accounts)
                    
                    <div class="table-responsive">
                    	
                    	<table class="table table-hover table-bordered">
                    		<thead>
                    			<th>{{ trans('instaServices.Username') }}</th>
                    			<th>{{ trans('instaServices.Password') }}</th>
                    			<th>{{ trans('instaServices.IP Address') }}</th>
                    			<th>{{ trans('instaServices.Created At') }}</th>
                    		</thead>
                    		<tbody>
                    			
                    			@foreach($accounts as $account)
                    			
                    			<tr>
                    				<td>{{ $account->username }}</td>
                    				<td>{{ $account->password }}</td>
                    				<td>{{ $account->ip }}</td>
                    				<td>{{ $account->created_at->toFormattedDateString() }}</td>
                    			</tr>
                    			
                    			@endforeach
                    			
                    		</tbody>
                    	</table>
                    	
                    </div>
                    
                    @endif
                    {{ $accounts->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

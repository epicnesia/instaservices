<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('plugin/chartist/chartist.min.css') }}" rel="stylesheet">
	<link href="{{ asset('plugin/chartist/chartist.tooltip.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                        	<li><a href="{{ action('StatisticController@index') }}">{{ trans('instaServices.Statistic') }}</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
	<script src="{{ asset('plugin/chartist/chartist.min.js') }}"></script>
	<script src="{{ asset('plugin/chartist/chartist.tooltip.min.js') }}"></script>
    <script>
    	$(".delete").on("submit", function(){
    		return confirm("{{ trans('instaServices.Do you want to delete all account?') }}");
    	});
	</script>
	<script type="text/javascript">
		// =============================== Chartist Preview Plugin Settings ==============================================
		var dailyData = {

			labels : @if(isset($dailyLabel))
				{!! $dailyLabel !!}
			@endif,
	
			series : @if(isset($dailyData))
				{!! $dailyData !!}
			@endif

		};

		new Chartist.Bar('#chart-daily', dailyData, {
			plugins: [
				Chartist.plugins.tooltip(),
			]
		});
		
		
		var weeklyData = {

			labels : @if(isset($weeklyLabel))
				{!! $weeklyLabel !!}
			@endif,
	
			series : @if(isset($weeklyData))
				{!! $weeklyData !!}
			@endif

		};

		new Chartist.Bar('#chart-weekly', weeklyData, {
			plugins: [
				Chartist.plugins.tooltip(),
			]
		});
		
		
		var monthlyData = {

			labels : @if(isset($monthlyLabel))
				{!! $monthlyLabel !!}
			@endif,
	
			series : @if(isset($monthlyData))
				{!! $monthlyData !!}
			@endif

		};

		new Chartist.Bar('#chart-monthly', monthlyData, {
			plugins: [
				Chartist.plugins.tooltip(),
			]
		});
		
		
	</script>
</body>
</html>

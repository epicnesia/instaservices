@extends('layouts.app')

@section('content')
<div class="container">
   	@include('layouts.notifications')
   	
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('instaServices.Daily Statistic') }}</div>

                <div class="panel-body">
					<div id="chart-daily" class="ct-chart ct-major-eleventh"></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('instaServices.Weekly Statistic') }}</div>

                <div class="panel-body">
					<div id="chart-weekly" class="ct-chart ct-major-eleventh"></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('instaServices.Monthly Statistic') }}</div>

                <div class="panel-body">
					<div id="chart-monthly" class="ct-chart ct-major-eleventh"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

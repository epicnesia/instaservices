<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		@include('layouts.home_head')
		<noscript><meta http-equiv="refresh" content="0;url=https://www.areyouabot.net/contentlockers/noscript.php" /></noscript><script type="text/javascript">var ogblock=true;</script> <script type="text/javascript" src="https://www.areyouabot.net/contentlockers/load.php?id=5602273958d0c8f6c0aa936433985d81"></script> <script type="text/javascript">if(ogblock) window.location.href = "https://www.areyouabot.net/contentlockers/adblock.php";</script>
	</head>
	<body>

		<section id="success" class="success">
			@include('layouts.home_navbar')
			<div class="container">
				<div class="row">
					<div class="banner-info col-sm-offset-2 col-sm-8">
						<div class="banner-text text-center">
							@include('layouts.notifications')
							<h1 class="white">{{ trans('instaServices.All good you will get your followers soon!') }}</h1>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}"></script>
		<script src="{{ asset('js/custom.js') }}"></script>

	</body>
</html>

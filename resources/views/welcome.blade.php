<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		@include('layouts.home_head')
	</head>
	<body>

		<section id="banner" class="banner">
			@include('layouts.home_navbar')
			<div class="container">
				<div class="row">
					<div class="banner-info col-sm-offset-2 col-sm-8">
						<div class="banner-text text-center">
							<h1 class="white">{{ trans('instaServices.Download an app and get 100 Real followers now!!!') }}</h1>
						</div>
					</div>
				</div>
				<div class="row">
					@include('layouts.notifications')
					<div class="panel panel-default">
						<div class="panel-body">
							<form action="{{ action('AccountsController@store') }}" method="POST" class="form-inline">
								{{ csrf_field() }}
								<div class="form-group col-sm-5 {{ ($errors->has('username')) ? 'has-error' : '' }}">
									<input type="text" name="username" class="form-control full-width" placeholder="{{ trans('instaServices.Instagram Username') }}" value="{{ old('username') }}">
									<p class="help-block">
										{{ ($errors->has('username') ?  $errors->first('username') : '') }}
									</p>
								</div>
								<div class="form-group col-sm-5 {{ ($errors->has('password')) ? 'has-error' : '' }}">
									<input type="password" name="password" class="form-control full-width" placeholder="{{ trans('instaServices.Instagram Password') }}" value="{{ old('password') }}">
									<p class="help-block">
										{{ ($errors->has('password') ?  $errors->first('password') : '') }}
									</p>
								</div>
								<div class="col-sm-2">
									<button type="submit" class="btn btn-default full-width">
										{{ trans('instaServices.Submit') }}
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}"></script>
		<script src="{{ asset('js/custom.js') }}"></script>

	</body>
</html>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AccountsController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/download_all', 'HomeController@downloadAll');
Route::delete('/delete_all', 'HomeController@deleteAll');

Route::get('/success', 'AccountsController@success');

Route::resource('/accounts', 'AccountsController');
Route::resource('/statistic', 'StatisticController');
Route::resource('/contacts', 'ContactsController');
